import React from 'react';
import Expo from 'expo';
import * as firebase from 'firebase';
import autobind from 'autobind-decorator'
import { Image } from 'react-native';
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right, Title } from 'native-base';

const CardView = ({onPress})=> {

    return (
      <Card>
        <CardItem>
          <Left>
            <Thumbnail source={require('./assets/ft1.jpg')} />
            <Body>
              <Text>NativeBase</Text>
              <Text note>GeekyAnts</Text>
            </Body>
          </Left>
        </CardItem>
        <CardItem cardBody button onPress={onPress}>
          <Image source={require('./assets/ft1.jpg')} style={{height: 200, width: null, flex: 1}}/>
        </CardItem>
        <CardItem>
          <Left>
            <Button transparent>
              <Icon active name="thumbs-up" />
              <Text>12 Likes</Text>
            </Button>
          </Left>
          <Body>
            <Button transparent>
              <Icon active name="chatbubbles" />
              <Text>4 Comments</Text>
            </Button>
          </Body>
          <Right>
            <Text>11h ago</Text>
          </Right>
        </CardItem>
      </Card>
    );
};

export default class TrucksListView extends React.Component {

  render(){
    return (
      <Container>
        <Header>
          <Left style={{flex: 1}}>
            <Button transparent onPress={()=> this.props.navigation.navigate("Home")}>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body style={{flex: 2, alignItems: 'center'}}>
            <Title>Trucks</Title>
          </Body>
          <Right style={{flex: 1}}>

          </Right>
        </Header>
        <Content>
            <CardView onPress={() => this.props.navigation.navigate('TruckView')} />
            <CardView onPress={() => this.props.navigation.navigate('TruckView')} />
            <CardView onPress={() => this.props.navigation.navigate('TruckView')} />
            <CardView onPress={() => this.props.navigation.navigate('TruckView')} />
        </Content>
      </Container>
    );
  }

}
