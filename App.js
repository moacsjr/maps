import React from 'react';
import Expo from 'expo';
import { Platform, View, StyleSheet, Button, Text, Image, Animated, Easing } from 'react-native';
import * as firebase from 'firebase';
import TrucksListView from './trucks'
import TruckView from './truckView'
import MapScreen from './MapView'
import autobind from 'autobind-decorator'
//import { Container, Header, Content, Button, Text } from 'native-base';
import {
  StackNavigator,SwitchNavigator
} from 'react-navigation';

import { MaterialCommunityIcons } from '@expo/vector-icons';

// Initialize Firebase
var config = {
    apiKey: "AIzaSyCDOt8Z--P-xQ8tq9Hpi-cJc1cNMVH26BY",
    authDomain: "foodtruck-1521909988492.firebaseapp.com",
    databaseURL: "https://foodtruck-1521909988492.firebaseio.com",
    projectId: "foodtruck-1521909988492",
    storageBucket: "foodtruck-1521909988492.appspot.com",
    messagingSenderId: "275470704183"
};
firebase.initializeApp(config);

const DEFAULT_PADDING = { top: 40, right: 40, bottom: 40, left: 40 };
const DEFAULT_GEOHASH = "7h2"
let email = 'moacsjr@gmail.com';
let password = 'matrix';

firebaseLogin = async ()=>{
    firebase.auth().signInWithEmailAndPassword(email, password).catch(function(error) {
        console.log(error.code);
        console.log(error.message);
    });

    firebase.auth().onAuthStateChanged(function(user) {
        console.log("User Login")
        if (user) {
            // User is signed in.
            var isAnonymous = user.isAnonymous;
            var uid = user.uid;

            //add some data
            // firebase.database().ref('locations/' + 1236456).set({
            //     name: "Buffet Caterina",
            //     longitude: -19.961298,
            //     latitude : -43.955589
            // });

        } else {
            // User is signed out.
            // ...
        }

    });
}


class LogoTitle extends React.Component {
  render() {
    return (
      <Image style={{
                      flex: 1,
                      alignSelf: 'stretch',
                      width: undefined,
                      height: undefined
                    }}
             source={require('./assets/perthfoodtrucks2.jpg')}
             resizeMode="contain"
             />
    );
  }
}

//---home-------//

const MenuItem = ({text, icon, style, onClick})=> (
  <Animated.View style={[styles.menuItem, style]}>

      <MaterialCommunityIcons name={icon} size={50} color="#d53636" style={{
                      flex: 1,
                      marginLeft: 10
                    }}
             />
      <Text onPress={()=> onClick()} style={{flex: 4, fontSize: 30}}>{text}</Text>

  </Animated.View>
)

class HomeScreen extends React.Component {

  componentWillMount = async () => {
      console.log("Loading...")
      await Expo.Font.loadAsync({
          'Roboto': require('native-base/Fonts/Roboto.ttf'),
          'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
      });
      console.log("Loaded")
      this.setState({load: true})
  }

  constructor(){
    super();
    this.animatedValue = new Animated.Value(0)
    this.animatedOpacityValue1 = new Animated.Value(0)
    this.animatedOpacityValue2 = new Animated.Value(0)
    this.animatedOpacityValue3 = new Animated.Value(0)
    this.animatedOpacityValue4 = new Animated.Value(0)
    this.animate.bind(this)
  }

  componentDidMount () {
    this.animate()
  }

  animate () {

    this.animatedValue.setValue(0)

    Animated.sequence([
      Animated.timing(
        this.animatedValue,
        {
          toValue: 3,
          duration: 500,
          delay:2000,
          easing: Easing.linear
        }
      ),
      Animated.timing(
        this.animatedOpacityValue1,
        {
          toValue: 1,
          duration: 100,
          delay:100,
          easing: Easing.linear
        }
      ),
      Animated.timing(
        this.animatedOpacityValue2,
        {
          toValue: 1,
          duration: 500,
          delay:500,
          easing: Easing.linear
        }
      ),
      Animated.timing(
        this.animatedOpacityValue3,
        {
          toValue: 1,
          duration: 500,
          delay:500,
          easing: Easing.linear
        }
      ),
      Animated.timing(
        this.animatedOpacityValue4,
        {
          toValue: 1,
          duration: 500,
          delay:500,
          easing: Easing.linear
        }
      )

    ])
    .start()

  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
            <LogoTitle />
        </View>

        <Animated.View style={{flex: this.animatedValue, justifyContent: 'flex-start', marginTop: 10, marginBottom: 10}}>
          <MenuItem text="Explorar" icon="map-marker" onClick={()=> this._goToMapsView()} style={{opacity: this.animatedOpacityValue1}} />
          <MenuItem text="Trucks" icon="food" onClick={this._goToTrucksView} style={{opacity: this.animatedOpacityValue2}} />
          <MenuItem text="Promoções" icon="star" style={{opacity: this.animatedOpacityValue3}} />
          <MenuItem text="Pontuação" icon="approval" style={{opacity: this.animatedOpacityValue4}} />
        </Animated.View>


      </View>
    )
  }

@autobind
  _goToMapsView(){
    this.props.navigation.navigate('Maps');
  }
@autobind
  _goToTrucksView(){
    this.props.navigation.navigate('Trucks');
  }
}

export default SwitchNavigator(
  {
    Home: HomeScreen,
    Maps: MapScreen,
    Trucks: TrucksListView,
    TrucksView: TruckView,
  },
  {
    initialRouteName: 'Home',
  }
);

const styles = StyleSheet.create({
  boddy: {
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  header: {
    flex: 2,
    height: 100,
    backgroundColor: '#F6BE35'
  },
  container:{
    backgroundColor: '#5c31a8',
    flex: 1,
    flexDirection: "column"
  },
  menuItem: {
    flex: 2,
    opacity: 0,
    backgroundColor: '#bf9ef7',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    borderRadius: 10,
    marginLeft: 15,
    marginRight: 15,
    marginTop: 10,
    marginBottom: 10

  }
});
