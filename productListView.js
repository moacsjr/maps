import React, { Component } from 'react';
import { Container, Header, Content, List, ListItem, Thumbnail, Text, Body } from 'native-base';
export default class ProductListView extends Component {
  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent>
              <Icon name='arrow-back' />
            </Button>
          </Left>
        </Header>
        <Content>
          <List>
            <ListItem>
              <Thumbnail square size={80} source={require('./assets/ft1.jpg')} />
              <Body>
                <Text>Maga Burger 1</Text>
                <Text note>Its time to build a difference . .</Text>
              </Body>
            </ListItem>
            <ListItem>
              <Thumbnail square size={80} source={require('./assets/ft1.jpg')} />
              <Body>
                <Text>Burger Sankhadeep</Text>
                <Text note>Its time to build a difference . .</Text>
              </Body>
            </ListItem>
            <ListItem>
              <Thumbnail square size={80} source={require('./assets/ft1.jpg')} />
              <Body>
                <Text>Pizza Suffer</Text>
                <Text note>Its time to build a difference . .</Text>
              </Body>
            </ListItem>
            <ListItem>
              <Thumbnail square size={80} source={require('./assets/ft2.png')} />
              <Body>
                <Text>Maga Stack</Text>
                <Text note>Its time to build a difference . .</Text>
              </Body>
            </ListItem>
            <ListItem>
              <Thumbnail square size={80} source={require('./assets/ft3.jpg')} />
              <Body>
                <Text>Sankhadeep</Text>
                <Text note>Its time to build a difference . .</Text>
              </Body>
            </ListItem>
            <ListItem>
              <Thumbnail square size={80} source={require('./assets/ft4.png')} />
              <Body>
                <Text>Lola Palusa</Text>
                <Text note>Its time to build a difference . .</Text>
              </Body>
            </ListItem>
          </List>
        </Content>
      </Container>
    );
  }
}
