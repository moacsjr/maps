import React from 'react';
import Expo from 'expo';
import { Platform, View, StyleSheet, Button, Text, Image, Animated, Easing } from 'react-native';
import * as firebase from 'firebase';
import autobind from 'autobind-decorator'


export default class MapScreen extends React.Component {

    state = { load: false }

    render(){
        var comp = (this.state.load)? <Maps /> : <View></View>
        return comp
    }
}

class Maps extends React.Component {

    state = {
        visitedRegions: [],
        nearByLocations: [],
        location: {
            latitude: -19.903566,
            longitude: -43.923267,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
        },
        userLocation: false,
        errorMessage: null,
    };

    constructor(){
        super();
        this.Geohash = new Geohash()
        this._loadLocations.bind(this)
        this.fitAllMarkers.bind(this)
        this.onRegionChange.bind(this)
        this.onMapPress.bind(this)
    }

    componentWillMount = async () => {
        await Expo.Font.loadAsync({
            'Roboto': require('native-base/Fonts/Roboto.ttf'),
            'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
        });
        if (Platform.OS === 'android' && !Expo.Constants.isDevice) {
            this.setState(this.state, {
                errorMessage: 'Oops, this will not work on Sketch in an Android emulator. Try it on your device!',
            });
        } else {
            this._getLocationAsync();
        }
    }

    _loadLocations =async () => {

        let _this = this;
        ref.once('value', function(snapshot) {
            let nearByLocations = [];
            snapshot.forEach(function(childSnapshot) {
                var childKey = childSnapshot.key;
                var childData = childSnapshot.val();
                var loc = {...childData}
                console.log("location >> ", loc)
                nearByLocations.push(childData);

            });
            console.log("State == ", nearByLocations)
            _this.setState({nearByLocations});
        });
    }

    _getLocationAsync = async () => {
        let { status } = await Expo.Permissions.askAsync(Expo.Permissions.LOCATION);
        if (status !== 'granted') {
            this.setState({
                errorMessage: 'Permission to access location was denied',
            });
        }

        let userLocation = await Expo.Location.getCurrentPositionAsync({
            enableHighAccuracy: false
        });
        console.log("User Location = ", userLocation)
        let geohash = this.Geohash.encode(userLocation.coords.latitude, userLocation.coords.longitude, 5)
        userLocation.geohash = geohash
        let newState = { userLocation }
        newState.regionHash = geohash

        // ref.orderByChild("geohash").equalTo(geohash).on("value", function(snapshot) {
        //     console.log("*query geohash ->",snapshot.key);
        // });

        this.setState(newState);
    };

    fitAllMarkers() {
        console.log("fitAllMarkers")
        let geohash = (this.state.userLocation)? this.state.userLocation.geohash : DEFAULT_GEOHASH
        console.log(geohash)
        let bounds = this.Geohash.bounds(geohash)
        let locations = [bounds.sw, bounds.ne].map(loc => { return {latitude: loc.lat, longitude: loc.lon}})
        console.log("Bounds locations to ", locations)

        this.map.fitToCoordinates([...locations], {
            edgePadding: DEFAULT_PADDING,
            animated: true,
        });
    }

    async onRegionChange(region) {
        console.log("OnRegionChange >> ", region.latitude, region.longitude)
        let _this = this
        let G = new Geohash()
        let geohash = G.encode(region.latitude, region.longitude, 5)
        if(!this.state.visitedRegions.includes(geohash)){
            region.geohash = geohash

            let newState = { region }
            console.log("Finding places in = ", geohash)
            newState.regionHash = geohash
            let places = await ref.orderByChild("geohash").equalTo(geohash).once("value")
            console.log("+query geohash ->",places.key);
            let nearByLocations = [];
            places.forEach(function(childPlace) {
                var key = childPlace.key;
                var place = childPlace.val();
                var loc = {...place}
                console.log("Found new location >> ", loc)
                nearByLocations.push(place);
            });
            nearByLocations = [...this.state.nearByLocations, ...nearByLocations]
            console.log("State == ", nearByLocations)
            let visitedRegions = [...this.state.visitedRegions, geohash]
            this.setState({region, nearByLocations, visitedRegions});
        }
    }

    onMapPress(e){
        let obj = e.nativeEvent.coordinate
        let G = new Geohash()
        let geohash = G.encode(obj.latitude, obj.longitude, 5)
        obj.geohash = geohash
        console.log(obj)
    }

    shouldComponentUpdate(){
        return true
    }

    render() {
        console.log("render this.state.nearByLocations.size = ", this.state.nearByLocations.length)
        return (
            <View style={{ flex: 1 }}>
                <Expo.MapView
                    ref={ref => { this.map = ref; }}
                    style={{ flex: 1 }}
                    customMapStyle={mapStyle}
                    provider="google"
                    showsUserLocation={true}
                    showsMyLocationButton={true}
                    showsCompass={true}
                    followsUserLocation={true}
                    onPress={(e) => this.onMapPress(e)}
                    onRegionChangeComplete={ r => this.onRegionChange(r)}
                >
                    {this.state.nearByLocations.map((location, index) => (
                        <Expo.MapView.Marker
                            key={index}
                            pinColor="blue"
                            image={require('./assets/flag2.png')}
                            title={location.name}
                            description="Lorem ipsum dolor sit amet"
                            coordinate={{
                                latitude: location.latitude,
                                longitude: location.longitude,
                            }}
                        />
                    ))}

                </Expo.MapView>
                <Button
                    onPress={e => this.fitAllMarkers()}
                    title="Learn More"
                    color="#841584"
                    accessibilityLabel="Learn more about this purple button"
                />
            </View>
        );
    }
}


class Geohash {

    constructor(){
        /* (Geohash-specific) Base32 map */
        this.base32 = '0123456789bcdefghjkmnpqrstuvwxyz';
        this.encode.bind(this)
        this.decode.bind(this)
        this.bounds.bind(this)
        this.adjacent.bind(this)
        this.neighbours.bind(this)
    }


    /**
     * Encodes latitude/longitude to geohash, either to specified precision or to automatically
     * evaluated precision.
     *
     * @param   {number} lat - Latitude in degrees.
     * @param   {number} lon - Longitude in degrees.
     * @param   {number} [precision] - Number of characters in resulting geohash.
     * @returns {string} Geohash of supplied latitude/longitude.
     * @throws  Invalid geohash.
     *
     * @example
     *     var geohash = Geohash.encode(52.205, 0.119, 7); // geohash: 'u120fxw'
     */
    encode(lat, lon, precision) {
        // infer precision?
        if (typeof precision == 'undefined') {
            // refine geohash until it matches precision of supplied lat/lon
            for (var p = 1; p <= 12; p++) {
                var hash = this.encode(lat, lon, p);
                var posn = this.decode(hash);
                if (posn.lat == lat && posn.lon == lon) return hash;
            }
            precision = 12; // set to maximum
        }

        lat = Number(lat);
        lon = Number(lon);
        precision = Number(precision);

        if (isNaN(lat) || isNaN(lon) || isNaN(precision)) throw new Error('Invalid geohash');

        var idx = 0; // index into base32 map
        var bit = 0; // each char holds 5 bits
        var evenBit = true;
        var geohash = '';

        var latMin = -90,
            latMax = 90;
        var lonMin = -180,
            lonMax = 180;

        while (geohash.length < precision) {
            if (evenBit) {
                // bisect E-W longitude
                var lonMid = (lonMin + lonMax) / 2;
                if (lon > lonMid) {
                    idx = idx * 2 + 1;
                    lonMin = lonMid;
                } else {
                    idx = idx * 2;
                    lonMax = lonMid;
                }
            } else {
                // bisect N-S latitude
                var latMid = (latMin + latMax) / 2;
                if (lat > latMid) {
                    idx = idx * 2 + 1;
                    latMin = latMid;
                } else {
                    idx = idx * 2;
                    latMax = latMid;
                }
            }
            evenBit = !evenBit;

            if (++bit == 5) {
                // 5 bits gives us a character: append it and start over
                geohash += this.base32.charAt(idx);
                bit = 0;
                idx = 0;
            }
        }

        return geohash;
    }

    /**
     * Decode geohash to latitude/longitude (location is approximate centre of geohash cell,
     *     to reasonable precision).
     *
     * @param   {string} geohash - Geohash string to be converted to latitude/longitude.
     * @returns {{lat:number, lon:number}} (Center of) geohashed location.
     * @throws  Invalid geohash.
     *
     * @example
     *     var latlon = Geohash.decode('u120fxw'); // latlon: { lat: 52.205, lon: 0.1188 }
     */
    decode(geohash) {

        var bounds = this.bounds(geohash); // <-- the hard work
        // now just determine the centre of the cell...

        var latMin = bounds.sw.lat,
            lonMin = bounds.sw.lon;
        var latMax = bounds.ne.lat,
            lonMax = bounds.ne.lon;

        // cell centre
        var lat = (latMin + latMax) / 2;
        var lon = (lonMin + lonMax) / 2;

        // round to close to centre without excessive precision: ⌊2-log10(Δ°)⌋ decimal places
        lat = lat.toFixed(Math.floor(2 - Math.log(latMax - latMin) / Math.LN10));
        lon = lon.toFixed(Math.floor(2 - Math.log(lonMax - lonMin) / Math.LN10));

        return {
            lat: Number(lat),
            lon: Number(lon)
        }
    }

    /**
     * Returns SW/NE latitude/longitude bounds of specified geohash.
     *
     * @param   {string} geohash - Cell that bounds are required of.
     * @returns {{sw: {lat: number, lon: number}, ne: {lat: number, lon: number}}}
     * @throws  Invalid geohash.
     */
    bounds(geohash) {
        if (geohash.length === 0) throw new Error('Invalid geohash');

        geohash = geohash.toLowerCase();

        var evenBit = true;
        var latMin = -90,
            latMax = 90;
        var lonMin = -180,
            lonMax = 180;

        for (var i = 0; i < geohash.length; i++) {
            var chr = geohash.charAt(i);
            var idx = this.base32.indexOf(chr);
            if (idx == -1) throw new Error('Invalid geohash');

            for (var n = 4; n >= 0; n--) {
                var bitN = idx >> n & 1;
                if (evenBit) {
                    // longitude
                    var lonMid = (lonMin + lonMax) / 2;
                    if (bitN == 1) {
                        lonMin = lonMid;
                    } else {
                        lonMax = lonMid;
                    }
                } else {
                    // latitude
                    var latMid = (latMin + latMax) / 2;
                    if (bitN == 1) {
                        latMin = latMid;
                    } else {
                        latMax = latMid;
                    }
                }
                evenBit = !evenBit;
            }
        }

        var bounds = {
            sw: {
                lat: latMin,
                lon: lonMin
            },
            ne: {
                lat: latMax,
                lon: lonMax
            }
        }

        return bounds;
    }

    /**
     * Determines adjacent cell in given direction.
     *
     * @param   geohash - Cell to which adjacent cell is required.
     * @param   direction - Direction from geohash (N/S/E/W).
     * @returns {string} Geocode of adjacent cell.
     * @throws  Invalid geohash.
     */
    adjacent(geohash, direction) {
        // based on github.com/davetroy/geohash-js

        geohash = geohash.toLowerCase();
        direction = direction.toLowerCase();

        if (geohash.length === 0) throw new Error('Invalid geohash');
        if ('nsew'.indexOf(direction) == -1) throw new Error('Invalid direction');

        var neighbour = {
            n: ['p0r21436x8zb9dcf5h7kjnmqesgutwvy', 'bc01fg45238967deuvhjyznpkmstqrwx'],
            s: ['14365h7k9dcfesgujnmqp0r2twvyx8zb', '238967debc01fg45kmstqrwxuvhjyznp'],
            e: ['bc01fg45238967deuvhjyznpkmstqrwx', 'p0r21436x8zb9dcf5h7kjnmqesgutwvy'],
            w: ['238967debc01fg45kmstqrwxuvhjyznp', '14365h7k9dcfesgujnmqp0r2twvyx8zb']
        };
        var border = {
            n: ['prxz', 'bcfguvyz'],
            s: ['028b', '0145hjnp'],
            e: ['bcfguvyz', 'prxz'],
            w: ['0145hjnp', '028b']
        };

        var lastCh = geohash.slice(-1); // last character of hash
        var parent = geohash.slice(0, -1); // hash without last character

        var type = geohash.length % 2;

        // check for edge-cases which don't share common prefix
        if (border[direction][type].indexOf(lastCh) != -1 && parent !== '') {
            parent = this.adjacent(parent, direction);
        }

        // append letter for direction to parent
        return parent + this.base32.charAt(neighbour[direction][type].indexOf(lastCh));
    }

    /**
     * Returns all 8 adjacent cells to specified geohash.
     *
     * @param   {string} geohash - Geohash neighbours are required of.
     * @returns {{n,ne,e,se,s,sw,w,nw: string}}
     * @throws  Invalid geohash.
     */
    neighbours(geohash) {
        return {
            'n': this.adjacent(geohash, 'n'),
            'ne': this.adjacent(this.adjacent(geohash, 'n'), 'e'),
            'e': this.adjacent(geohash, 'e'),
            'se': this.adjacent(this.adjacent(geohash, 's'), 'e'),
            's': this.adjacent(geohash, 's'),
            'sw': this.adjacent(this.adjacent(geohash, 's'), 'w'),
            'w': this.adjacent(geohash, 'w'),
            'nw': this.adjacent(this.adjacent(geohash, 'n'), 'w')
        }
    }
}



//****** maps

const mapStyle = [
    {
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#ebe3cd"
            }
        ]
    },
    {
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#523735"
            }
        ]
    },
    {
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "color": "#f5f1e6"
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#c9b2a6"
            }
        ]
    },
    {
        "featureType": "administrative.land_parcel",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#dcd2be"
            }
        ]
    },
    {
        "featureType": "administrative.land_parcel",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#ae9e90"
            }
        ]
    },
    {
        "featureType": "landscape.natural",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#dfd2ae"
            }
        ]
    },
    {
        "featureType": "poi",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#dfd2ae"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#93817c"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#a5b076"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#447530"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f5f1e6"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#fdfcf8"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f8c967"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#e9bc62"
            }
        ]
    },
    {
        "featureType": "road.highway.controlled_access",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#e98d58"
            }
        ]
    },
    {
        "featureType": "road.highway.controlled_access",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#db8555"
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#806b63"
            }
        ]
    },
    {
        "featureType": "transit",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit.line",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#dfd2ae"
            }
        ]
    },
    {
        "featureType": "transit.line",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#8f7d77"
            }
        ]
    },
    {
        "featureType": "transit.line",
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "color": "#ebe3cd"
            }
        ]
    },
    {
        "featureType": "transit.station",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#dfd2ae"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#b9d3c2"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#92998d"
            }
        ]
    }
]
